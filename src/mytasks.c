/*
 * mytasks.c
 */
#include "mytasks.h"


void vLEDFlashTask1( void *pvParameters )
{
  //portTickType xLastWakeTime;
  //const portTickType xFrequency = 1000;
  //xLastWakeTime=xTaskGetTickCount();
    for( ;; )
    {
	  LEDToggle(1);
	  vTaskDelay(pdMS_TO_TICKS(100));
	  //vTaskDelayUntil(&xLastWakeTime,xFrequency);
    }
}

void vLEDFlashTask2( void *pvParameters )
{
  //portTickType xLastWakeTime;
  //const portTickType xFrequency = 1000;
  //xLastWakeTime=xTaskGetTickCount();
    for( ;; )
    {
	  LEDToggle(2);
	  vTaskDelay(pdMS_TO_TICKS(500));
	  //vTaskDelayUntil(&xLastWakeTime,xFrequency);
    }
}

void vLEDFlashTask3( void *pvParameters )
{
  //portTickType xLastWakeTime;
  //const portTickType xFrequency = 1000;
  //xLastWakeTime=xTaskGetTickCount();
    for( ;; )
    {
	  LEDToggle(3);
	  vTaskDelay(pdMS_TO_TICKS(1000));
	  //vTaskDelayUntil(&xLastWakeTime,xFrequency);
    }
}


void vLEDBlipTask( void *pvParameters )
{
  //portTickType xLastWakeTime;
  //const portTickType xFrequency = 1000;
  //xLastWakeTime=xTaskGetTickCount();
    for( ;; )
    {
	  LEDOn(3);
	  vTaskDelay(pdMS_TO_TICKS(20));
	  LEDOff(3);
	  vTaskDelay(pdMS_TO_TICKS(100));
	  LEDOn(3);
	  vTaskDelay(pdMS_TO_TICKS(20));
	  LEDOff(3);
	  vTaskDelay(pdMS_TO_TICKS(1000));
	  //vTaskDelayUntil(&xLastWakeTime,xFrequency);
    }
}
