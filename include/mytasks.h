/*
 * mytasks.h
 */

#ifndef MYTASKS_H_
#define MYTASKS_H_
#include "stm32f10x.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "leds.h"
void vLEDFlashTask1( void *pvParameters );
void vLEDFlashTask2( void *pvParameters );
void vLEDFlashTask3( void *pvParameters );
void vLEDBlipTask( void *pvParameters );
#endif /* MYTASKS_H_ */
