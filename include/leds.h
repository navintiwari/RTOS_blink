/*
 * leds.h
 *
 *  Created on: Nov 3, 2011
 *      Author: MMM
 */

#ifndef LEDS_H_
#define LEDS_H_
#include "stm32f10x.h"
#define LED1 			GPIO_Pin_0
#define LED2 			GPIO_Pin_2
#define LED3 			GPIO_Pin_3
#define LED4 			GPIO_Pin_4
#define LED5 			GPIO_Pin_5
#define LEDPORT			GPIOE
#define LEDPORTCLK		RCC_APB2Periph_GPIOE
//function prototypes
void LEDsInit(void);
void LEDOn(uint32_t);
void LEDOff(uint32_t);
void LEDToggle(uint32_t);
#endif /* LEDS_H_ */
